#!/bin/bash 
# 初始化运行环境
# 安装jak、tomcat
# 创建存放下载文件的文件夹 /readyFile
echo '开始安装jdkandtomcat'
mkdir /readyFile
cd /readyFile
# 下载jdk并安装
wget http://down.zvo.cn/centos/jdk-8u73-linux-x64.rpm
rpm -ivh jdk-8u73-linux-x64.rpm
echo 'export JAVA_HOME=/usr/java/jdk1.8.0_73/'>>/etc/profile
echo 'export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar'>>/etc/profile
echo 'export PATH=$PATH:$JAVA_HOME/bin'>>/etc/profile
export JAVA_HOME=/usr/java/jdk1.8.0_73/
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
# 下载Tomcat8并解压处理好
mkdir /readyFile/tomcat8
cd /readyFile/tomcat8
wget http://down.zvo.cn/centos/apache-tomcat-8.5.51.zip
# 安装 unzip 命令
yum -y install unzip
unzip apache-tomcat-8.5.51.zip
cd webapps
rm -rf docs
rm -rf examples
rm -rf host-manager
rm -rf manager
cd ROOT
rm -rf ./*
# 将tomcat8移动到/mnt目录下
cd /readyFile
mv tomcat8/ /mnt/
# 将Tomcat8加入开机自启动
echo 'JAVA_HOME=/usr/java/jdk1.8.0_73/'>>/etc/rc.local
echo 'export JAVA_HOME'>>/etc/rc.local
# 加入开机启动文件
echo '/mnt/tomcat8/bin/startup.sh'>>/etc/rc.d/rc.local
# 赋予可执行权限
chmod +x /mnt/tomcat8/bin/startup.sh
chmod +x /etc/rc.d/rc.local
# 下载部署最新的wangmarket安装包
cd /readyFile
wget http://down.zvo.cn/wangmarket/wangmarket.zip
cp wangmarket.zip /mnt/tomcat8/webapps/ROOT/wangmarket.zip
cd /mnt/tomcat8/webapps/ROOT/
unzip wangmarket.zip
rm -rf wangmarket.zip
# 环境准备结束
echo 'jdkandtomcat安装完成'
sleep 3
cd /root
# 安装mysql
echo '开始安装mysql'
#移除mariadb数据库
yum -y remove mariadb-libs.x86_64
# 解决某些服务器安装异常：源 "MySQL 5.7 Community Server" 的 GPG 密钥已安装，但是不适用于此软件包。请检查源的公钥 URL 是否配置正确。
rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
#下载mysql源安装包
wget http://repo.mysql.com/mysql57-community-release-el7-8.noarch.rpm
# 安装mysql源
yum -y localinstall mysql57-community-release-el7-8.noarch.rpm
#安装相关依赖
yum -y install gcc gcc-c++ gcc-g77 autoconf automake zlib* fiex* libxml* ncurses-devel libmcrypt* libtool-ltdl-devel* make cmake bison git openssl openssl-devel
#安装mysql
yum -y install mysql-community-server
#启动mysql
systemctl start mysqld
#开机启动
systemctl enable mysqld
systemctl daemon-reload
#开启防火墙
systemctl start firewalld
#放开3306端口访问权限
firewall-cmd --zone=public --add-port=3306/tcp --permanent
#重新载入防火强
firewall-cmd --reload
#设置完后重启mysql服务
systemctl restart mysqld
echo 'mysql安装完成'
sleep 3
# 一些变量
tomcat_home=/mnt/tomcat8
SHUTDOWN=$tomcat_home/bin/shutdown.sh
STARTTOMCAT=$tomcat_home/bin/startup.sh
# wangmarket用户名
mysql_user_wangmarket=wangmarket
# 临时变量，用于截取root初始密码
mysql_oldpwd_temp=`grep 'temporary password' /var/log/mysqld.log`
# root用户初始密码
mysql_oldpwd=${mysql_oldpwd_temp#*root@localhost: }
# root数据库密码 12位随机字符串+123，
mysql_pwd=`openssl rand -base64 9`123,
# wangmarket用户密码 12位随机字符串+123，
mysql_pwd_wangmarket=`openssl rand -base64 9`123,
# 数据库名称
db_name=wangmarket
# 配置文件地址
txt_path=/root/account.txt

# 安装完成，修改root密码
mysql -uroot -p$mysql_oldpwd --connect-expired-password -e "set password=password('$mysql_pwd');"
# mysql创建wangmarket用户
mysql -uroot -p$mysql_pwd -e "create user '$mysql_user_wangmarket'@'%' IDENTIFIED BY '$mysql_pwd_wangmarket'"
# 修改网市场配置文件中的连接账密
sed -i "s#^spring.datasource.username=.*#spring.datasource.username=$mysql_user_wangmarket#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i "s#^spring.datasource.password=.*#spring.datasource.password=$mysql_pwd_wangmarket#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
# 创建 wangmarket 数据库
mysql -uroot -p$mysql_pwd -e "create database IF NOT EXISTS ${db_name}"
# 导入sql
cd /root
wget https://gitee.com/mail_osc/wangmarket/raw/master/else/wangmarket.sql
mysql -uroot -p$mysql_pwd $db_name -e "source /root/wangmarket.sql"
# wangmarket用户赋予远程连接权限
mysql -uroot -p$mysql_pwd -e "grant all privileges on $db_name.* to '$mysql_user_wangmarket'@'%' identified by '$mysql_pwd_wangmarket'"

#
# 自动修改wangmarket中的system数据表中的数据
#
# 配置安装域名相关-泛解析域名
wget down.zvo.cn/properties/properties-1.0.1.jar -O ~/properties.jar
install_domain=`java -cp ~/properties.jar Properties -path=/root/install.properties -get domain`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='http://$install_domain/' WHERE name = 'MASTER_SITE_URL'"
# 配置安装域名相关-cdn附件域名
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='cdn.$install_domain' WHERE name = 'ATTACHMENT_FILE_URL'"
# 配置存储方式为华为云OBS存储
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='huaWeiYunOBS' WHERE name = 'ATTACHMENT_FILE_MODE'"
# 配置华为云AK
install_accesskeyid=`java -cp ~/properties.jar Properties -path=/root/install.properties -get accesskey.id`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='$install_accesskeyid' WHERE name = 'HUAWEIYUN_ACCESSKEYID'"
mysql -uroot -p$mysql_pwd $db_name -e "INSERT INTO system(name,value) VALUES('HUAWEIYUN_ACCESSKEYID', '$install_accesskeyid')"
install_accesskeysecret=`java -cp ~/properties.jar Properties -path=/root/install.properties -get accesskey.secret`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='$install_accesskeysecret' WHERE name = 'HUAWEIYUN_ACCESSKEYSECRET'"
mysql -uroot -p$mysql_pwd $db_name -e "INSERT INTO system(name,value) VALUES('HUAWEIYUN_ACCESSKEYSECRET', '$install_accesskeysecret')"
# 配置华为云OBS - bucketname
install_obsbucket=`java -cp ~/properties.jar Properties -path=/root/install.properties -get obs.bucket`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='$install_obsbucket' WHERE name = 'HUAWEIYUN_OBS_BUCKETNAME'"
mysql -uroot -p$mysql_pwd $db_name -e "INSERT INTO system(name,value) VALUES('HUAWEIYUN_OBS_BUCKETNAME', '$install_obsbucket')"
# 配置华为云OBS - endpoint ，格式如 obs.ap-southeast-1.myhuaweicloud.com
install_obsendpoint=`java -cp ~/properties.jar Properties -path=/root/install.properties -get obs.endpoint`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='$install_obsendpoint' WHERE name = 'HUAWEIYUN_OBS_ENDPOINT'"
mysql -uroot -p$mysql_pwd $db_name -e "INSERT INTO system(name,value) VALUES('HUAWEIYUN_OBS_ENDPOINT', '$install_obsendpoint')"
# 配置华为云可用区，如 ap-southeast-1
install_region=`java -cp ~/properties.jar Properties -path=/root/install.properties -get region`
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='$install_region' WHERE name = 'HUAWEIYUN_COMMON_ENDPOINT'"
mysql -uroot -p$mysql_pwd $db_name -e "INSERT INTO system(name,value) VALUES('HUAWEIYUN_COMMON_ENDPOINT', '$install_region')"
# 禁止通过 /install/index.do 在安装系统
mysql -uroot -p$mysql_pwd $db_name -e "UPDATE system SET value='false' WHERE name = 'IW_AUTO_INSTALL_USE'"

# 输出mysql配置信息到/root/account.txt
touch /root/account.txt
echo 'mysql5.7' >> $txt_path
echo '数据库名称:'$db_name >> $txt_path
echo ' ' >> $txt_path
echo 'root总账号,安全考虑只能本机用，无法通过外网登录' >> $txt_path
echo 'username:root' >> $txt_path
echo 'password:'$mysql_pwd >> $txt_path
echo ' ' >> $txt_path
echo 'wangmarket所使用的：' >> $txt_path
echo 'username:'$mysql_user_wangmarket >> $txt_path
echo 'password:'$mysql_pwd_wangmarket >> $txt_path
echo '这个可以通过外网进行登录管理' >> $txt_path
systemctl stop firewalld.service
# 启动tomcat
echo "启动$tomcat_home"
$STARTTOMCAT
#看启动日志
#echo "看启动日志"
#tail -f $tomcat_home/logs/catalina.out
