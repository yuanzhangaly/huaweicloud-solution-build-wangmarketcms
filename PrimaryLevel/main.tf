#
# 命名规则上，采用 项目名+云模块名+时间戳 的形式。保证命名唯一的同时，也便于通过控制台一眼能认出开通的哪个云产品是属于哪个项目的，不至于一个账号上运行多个不同的项目而混乱
# 项目地址： https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms
#
terraform {
        required_providers {
                huaweicloud = {
                        source  = "huawei.com/provider/huaweicloud"
                        version = "1.38.2"
                }
        }
}

# 定义一些变量，时间戳的作用是避免命名重复。 使用时调用如： ${local.timestamp}
locals {
	timestamp = formatdate("YYYYMMDDhhmmss", timestamp())
	# ecs 服务器的密码
	ecspassword="wangmarket#${local.timestamp}"
}

# VPC虚拟私有云。对应控制台 https://console.huaweicloud.com/vpc/
resource "huaweicloud_vpc" "vpc" {
  name = "wangmarket_vpc_${local.timestamp}"
  cidr = "192.168.0.0/16"
}
# 子网
resource "huaweicloud_vpc_subnet" "subnet" {
  name       = "wangmarket_subnet_${local.timestamp}"
  cidr       = "192.168.0.0/16"
  gateway_ip = "192.168.0.1"
  vpc_id     = huaweicloud_vpc.vpc.id
}
# 创建安全组
resource "huaweicloud_networking_secgroup" "secgroup"{
	name = "wangmarket_secgroup_${local.timestamp}"
}
# 安全组规则-开放80端口
resource "huaweicloud_networking_secgroup_rule" "secgroup_rule_80"{
	direction         = "ingress"
	ethertype         = "IPv4"
	protocol          = "tcp"
	ports	          = 80
	remote_ip_prefix  = "0.0.0.0/0"
	security_group_id = huaweicloud_networking_secgroup.secgroup.id
}
# 安全组规则-开放22端口
resource "huaweicloud_networking_secgroup_rule" "secgroup_rule_22"{
        direction         = "ingress"
        ethertype         = "IPv4"
        protocol          = "tcp"
        ports             = 22  
        remote_ip_prefix  = "0.0.0.0/0"
        security_group_id = huaweicloud_networking_secgroup.secgroup.id
}
# 安全组规则-开放443端口
resource "huaweicloud_networking_secgroup_rule" "secgroup_rule_443"{
        direction         = "ingress"
        ethertype         = "IPv4"
        protocol          = "tcp"
        ports             = 443  
        remote_ip_prefix  = "0.0.0.0/0"
        security_group_id = huaweicloud_networking_secgroup.secgroup.id
}
# 安全组规则-开放3306端口
resource "huaweicloud_networking_secgroup_rule" "secgroup_rule_3306"{
        direction         = "ingress"
        ethertype         = "IPv4"
        protocol          = "tcp"
        ports             = 3306
        remote_ip_prefix  = "0.0.0.0/0"
        security_group_id = huaweicloud_networking_secgroup.secgroup.id
}
# 安全组规则-允许ping程序测试弹性云服务器的连通性
resource "huaweicloud_networking_secgroup_rule" "secgroup_rule_ping"{
        direction         = "ingress"
        ethertype         = "IPv4"
        protocol          = "icmp"
	remote_ip_prefix  = "0.0.0.0/0"
        security_group_id = huaweicloud_networking_secgroup.secgroup.id
}

# 创建弹性公网EIP
# 按流量计费方式，10MB带宽
resource "huaweicloud_vpc_eip" "eip" {
  name = "wangmarket_eip_${local.timestamp}"
  publicip {
    type = "5_bgp"
  }
  bandwidth {
    share_type  = "PER"
    name        = "wangmarket_bandwidth_${local.timestamp}"
    size        = 10
    charge_mode = "traffic"
  }
}

# 搜索CentOS7.4的镜像
data "huaweicloud_images_image" "image" {
  most_recent = true
  visibility  = "public"
  name        = "CentOS 7.4 64bit"
}
# 搜索服务器规格,1核1G内存的
data "huaweicloud_availability_zones" "myaz" {}
data "huaweicloud_compute_flavors" "myflavor" {
  availability_zone = data.huaweicloud_availability_zones.myaz.names[0]
  performance_type  = "normal"
  cpu_core_count    = 1
  memory_size       = 1
}
# 创建服务器
# 注意有坑：设置了user_data字段后，admin_pass字段将无效
resource "huaweicloud_compute_instance" "instance" {
  name              = "wangmarket_${local.timestamp}"
 #admin_pass   = "wngMarket#23"
  image_id         = data.huaweicloud_images_image.image.id
  flavor_id          = data.huaweicloud_compute_flavors.myflavor.ids[0]
  availability_zone = data.huaweicloud_availability_zones.myaz.names[0]
  security_group_ids= [huaweicloud_networking_secgroup.secgroup.id]

  network {
    uuid = huaweicloud_vpc_subnet.subnet.id
  }
  user_data           = "#!/bin/sh\ncd ~\necho ${local.ecspassword} | passwd root --stdin > /dev/null 2>&1\ntouch /root/install.properties\necho 'ecs.ip=${huaweicloud_vpc_eip.eip.address}'>>/root/install.properties\necho 'ecs.username=root'>>/root/install.properties\necho 'ecs.password=${local.ecspassword}'>>/root/install.properties\nwget http://down.zvo.cn/wangmarket/version/v5.6/wangmarket_install_20220811_1730.sh -O wangmarket_install.sh\nchmod -R 777 wangmarket_install.sh\nchmod -x wangmarket_install.sh\nsh wangmarket_install.sh"
}

# 将服务器跟弹性公网绑定
resource "huaweicloud_compute_eip_associate" "associated" {
  public_ip   = huaweicloud_vpc_eip.eip.address
  instance_id = huaweicloud_compute_instance.instance.id
}

# 输出ecs服务器的公网ip
output "ecsip"{
	value = huaweicloud_vpc_eip.eip.address
}
# 输出ecs服务器的root用户ssh远程登录的密码
output "ecspassword"{
	value = local.ecspassword
}