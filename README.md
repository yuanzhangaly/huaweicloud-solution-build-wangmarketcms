## 介绍
基于华为云应用编排的网市场云建站系统，SAAS CMS的一键部署解决方案。

## 该解决方案有何用途？

该解决方案基于华为云服务器，帮助您快速部署您自己的SAAS云建站系统。部署好后，您可通过登录管理后台，即可在线开通网站及管理网站。所有操作过程全部无需任何后端技术人员及服务器运维人员参与。

#### 配置规格

提供入门级、标准级、增强级三个规格：

* **入门级**，搭配 1U1G 云服务器 + 1Mbit 带宽; （无需设置一键部署,想快速体验功能试试效果必须选这个）
* **标准级**，搭配 1U2G 云服务器 + 1Mbit 带宽 + OBS + CDN；（需设置域名并进行解析操作）
* **增强级**，搭配 2U4G 云服务器 + 5Mbit 带宽 + OBS + CDN + Mysql 2U4G；（需设置域名并进行解析操作）

#### 适用场景

* 您是互联网公司，有建站业务，需要给客户做网站
* 您是企业服务公司，如代理记账、人力资源，想充分挖掘老客户，增加建站的增值服务
* 您是华为云分销商，在企业没有上云需求的情况下，通过您自己部署的云建站系统在线给客户开通一个网站，然后帮客户开通华为云账户，购买个域名。

## 方案架构

该解决方案可以帮助您在华为云弹性云服务器 Linux 操作系统中，快速搭建SAAS云建站系统环境，通过云上提供的OBS对象存储的无限存储及CDN无带宽上限的能力，用最低的费用，达到最快的响应及网站打开速度。

#### 架构描述

该解决方案部署如下资源：

1. 创建[弹性云服务器](https://www.huaweicloud.com/product/ecs.html)，自动部署开源 wangmarket 系统，提供SAAS云建站系统服务。
2. 安全组可以保护后端云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。
3. [云数据库 RDS](https://www.huaweicloud.com/product/mysql.html) 主备部署，提供具备跨可用区故障容灾能力的高可用数据库。
4. 创建OBS对象存储桶，用来存储网站中所上传的图片、附件等资源。
5. 创建CDN加速，用来配合OBS实现网站图片及附件资源的网络分发。
6. 创建LTS云日志服务，用来监控网站访问情况及记录所有后台操作记录。

#### 方案优势

* 一台服务器承载成千上万个网站，不需再为每开通一个网站就要单独付费
* OBS对象存储，具备价格低、安全高、且随着网站及内容增加，具备无限扩容的能力
* CDN具备使网站极速打开的能力，不再局限于服务器带宽。
* 云数据库 RDS 主备部署，具备跨可用区故障容灾能力，保障数据安全。
* 一键部署，15 分钟即可快速完成自有云上SAAS云建站平台构建。

## 目录及文件说明
* **PrimaryLevel/** 入门级
	* **main.tf** 资源编排文件，用于执行自动化部署操作 
	* **user_data_install.sh** 创建服务器后，自动执行安装的shell命令，用于安装tomcat、mysql、配置环境、项目等。
* **StandardLevel/** 标准级  
	* **main.tf** 资源编排文件，用于执行自动化部署操作 
	* **user_data_install.sh** 创建服务器后，自动执行安装的shell命令
* **EnhanceLevel/** 增强级  
	* **main.tf** 资源编排文件，用于执行自动化部署操作 
	* **user_data_install.sh** 创建服务器后，自动执行安装的shell命令
* **ak.sh** 开发调试使用，用于设置自己的华为云ak，然后直接通过执行 source ak.sh 即可设置环境变量  
* **README.md** 说明文件  

## 本地开发调试

#### 1. 本地terraform环境
[安装Terraform](https://support.huaweicloud.com/qs-terraform/index.html) | 
[配置华为云 provider](https://support.huaweicloud.com/terraform_faq/index.html)

#### 2. 本地创建测试准备
创建一个新的目录并进入，然后执行以下进行拉取相关项目文件

````
# 下载 versions 版本定义
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/PrimaryLevel/local_versions.tf -O versions.tf
# 下载编排文件
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/PrimaryLevel/main.tf -O main.tf
# 下载用于本地环境变量定义的shell文件
wget https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-wangmarketcms/raw/master/PrimaryLevel/ak.sh -O ak.sh
# 赋予权限
# chmod -R 777 ak.sh

````

#### 3. 设置 ak.sh
修改 ak.sh 文件，将 HW_ACCESS_KEY、HW_SECRET_KEY ，填上自己的

````
# HW_REGION_NAME：区域，即需要创建管理哪个区域的资源。
export HW_REGION_NAME="cn-north-4"

# HW_ACCESS_KEY：密钥ID，即AK。查询方法请参见 https://support.huaweicloud.com/usermanual-ca/ca_01_0003.html
export HW_ACCESS_KEY="my-access-key"

# HW_SECRET_KEY：访问密钥，即SK。查询方法请参见 https://support.huaweicloud.com/usermanual-ca/ca_01_0003.html
export HW_SECRET_KEY="my-secret-key"
````

#### 4. 执行 ak.sh 设置环境变量
执行 ak.sh 文件，来快速设置环境变量 

````
source ak.sh
````

#### 5. 使用 main.tf 创建资源
按顺序逐个执行以下命令进行创建资源

````
terraform init
````


````
terraform plan
````

````
terraform apply
````
