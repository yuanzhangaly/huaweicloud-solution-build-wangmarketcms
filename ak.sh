# HW_REGION_NAME：区域，即需要创建管理哪个区域的资源。您可以在这里查询华为云支持的区域。
export HW_REGION_NAME="cn-north-4"
# HW_ACCESS_KEY：密钥ID，即AK。查询方法请参见 https://support.huaweicloud.com/usermanual-ca/ca_01_0003.html
export HW_ACCESS_KEY="my-access-key"
# HW_SECRET_KEY：访问密钥，即SK。查询方法请参见 https://support.huaweicloud.com/usermanual-ca/ca_01_0003.html
export HW_SECRET_KEY="my-secret-key"